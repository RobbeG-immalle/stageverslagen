# StageVerslagen
In dit project staan de dagverslagen van mijn observatiestage.

# 2018-10-01
Vandaag, de eerste dag in mijn stagebedrijf SUEZ. Toen ik aankwam was het eerste dat me opviel dat het een zeer groot bedrijf was.
Toen ik aankwam heb ik bij de receptie mijn stagebegeleider Gerrit ontmoet die mij nadien een bureau toegewezen heeft. Degene die de bureau normaal gebruikt had deze week verlof.
We hebben het gehad over de planning deze week, woensdag zou ik al mee op uitstap mogen naar Nederland voor een meeting met KPN.
Ik heb vandaag ook geholpen een switch en access point te installeren. Ik heb ook nog opdrachtjes mogen uitvoeren met wireshark. En ben naar 2 datacenters gaan kijken!

# 2018-10-02
Ik ben deze keer om 8 uur op het bedrijf aangekomen en ben onmiddelijk naar mijn bureau gegaan. Mijn stagementor was ook net aangekomen en gaf mij onmiddelijk een taak.
Ik mocht op excel een lijst maken van alle dorpen in België, Nederland en Duitsland die een switch hebben van Suez en de hoeveelheid switches die ze daar hadden.

# 2018-10-03
De derde dag op stage. Om 2 uur vertrok ik naar Nederland met Gerrit voor een meeting met KPN in Houten. 
Het was heel interessant om eens een meeting bij te wonen ookal begreep ik grotendeels niet wat er gezegd werd :).
Na de meeting ben ik door mijn stagebegeleider thuis afgezet.

# 2018-10-04
Ik ben vandaag om 7 uur opgehaald door mijn stagebegeleider om nogmaals naar Nederland te gaan, deze keer naar een Suez vestiging in Helmond. De vestiging was heel groot. 
Ter plaatse heb ik ongeveer hetzelfde gedaan als de vorige dagen, maar ik heb ook een paar meetingen bijgewoond.
Rond 4 uur keerden we weer naar huis en heeft Gerrit me thuis afgezet.

# 2018-10-05
De laatste stagedag. Ik heb mijn stagebegeleider een kleine attentie gegeven waar hij denk ik heel blij mee was.
Ik heb nog wat verdergewerkt aan mijn casestudy met behulp van een van de opdrachten die ik gemaakt heb tijdens de stage.
Ik heb vandaag ook een beetje aan de website voor het gip gewerkt.


